set runtimepath+=~/vim-dotfiles
set runtimepath+=~/vim-dotfiles/.vim

runtime .vimrc
let g:vimtex_view_general_viewer = 'SumatraPDF'
