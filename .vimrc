" Initial config
set encoding=utf-8

call plug#begin('~/vim-dotfiles/.vim/bundle')

Plug 'w0rp/ale'
Plug 'jiangmiao/auto-pairs'
Plug 'tomasr/molokai'
Plug 'jaredgorski/spacecamp'
Plug 't1mxg0d/vim-lucario'
Plug 'mbbill/undotree'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'plasticboy/vim-markdown'
Plug 'vimwiki/vimwiki'
Plug 'lervag/vimtex'

call plug#end()

" Quality of life(tm)
set hidden
set confirm
set wildmenu
set showcmd
set hlsearch
set ignorecase
set smartcase
set number
set ruler
set laststatus=2
set nostartofline
set mouse=a
set timeoutlen=1000 ttimeoutlen=100
set pastetoggle=<F11>
set updatetime=500
set ttyfast
set visualbell
set t_vb=

set backspace=indent,eol,start
set autoindent
set shiftwidth=4
set softtabstop=4
set expandtab


" Theme config
set background=dark
let g:alduin_Shout_Aura_Whisper = 1
let g:alduin_Shout_Fire_Breath = 0
let g:alduin_Shout_Animal_Allegiance = 1
let g:alduin_Shout_Dragon_Aspect = 0
colorscheme alduin
let g:airline_theme='alduin'
"hi Normal ctermbg=none
"hi Comment ctermfg=lightgrey
""hi LineNr term=bold cterm=NONE ctermfg=246 ctermbg=236 gui=NONE guifg=DarkGrey guibg=NONE
"hi Visual ctermbg=8
"hi CursorLine ctermbg=234 guibg=gray8


" Plugin config

""" undotree
map <Leader>u :UndotreeToggle<CR>

""" nerdtree
map <Leader>t :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

""" vimwiki
let g:vimwiki_list = [{'path': '~/markdown/',
                     \ 'syntax': 'markdown', 'ext': '.mdwiki'}]

""" airline
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.branch = '⎇'
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#fnamemod = ':t'

""" syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_auto_jump = 0
let g:syntastic_python_checkers = ['flake8','pylint']

""" ale
let g:ale_linters = {
\   'python': ['flake8'],
\}
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1

""" vimtex
let g:tex_flavor='latex'


" Keymaps
" let mapleader = ' '
map Y y$
nnoremap <Leader>s :nohlsearch<CR>
noremap <C-c> <Esc>
inoremap <C-c> <Esc>

""" window and tab navigation
nnoremap <A-h> <C-w>h
tnoremap <A-h> <C-\><C-N><C-w>h
nnoremap <A-j> <C-w>j
tnoremap <A-j> <C-\><C-N><C-w>j
nnoremap <A-k> <C-w>k
tnoremap <A-k> <C-\><C-N><C-w>k
nnoremap <A-l> <C-w>l
tnoremap <A-l> <C-\><C-N><C-w>l

nnoremap <A-,> :bp<CR>
tnoremap <A-,> <C-\><C-N>:bp<CR>
nnoremap <A-.> :bn<CR>
tnoremap <A-.> <C-\><C-N>:bn<CR>

nnoremap <A-q> :bd<CR>
tnoremap <A-q> <C-\><C-N>:bd<CR>

nmap <Leader>n :enew<CR>

""" clipboard maps
map gy "+y
map gp "+p

""" arrow keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

